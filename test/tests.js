
const request = require('request');
const expect = require('chai').expect;
const chai = require('chai');
const sinon = require('sinon');
const assert = require("assert");
const mocha = require("mocha");
let  istanbul = require('istanbul');

const addTwoNumbers = require('../utils/addTwoNumbers.js');

describe('Test test', function () {
    it('should add two numbers', function () {

        // 1. ARRANGE
        var x = 5;
        var y = 1;
        var sum1 = x + y;

        // 2. ACT
        var sum2 = addTwoNumbers(x, y);

        // 3. ASSERT
        expect(sum2).to.be.equal(sum1);

    });

    it('Should assert true to be true', () => {
        expect(true).to.be.true;
    });

    it("checks equality", function() {
        assert.equal(true, true);
    });
});



