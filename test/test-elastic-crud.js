//needed for the logging module

global.__basedir = "./";

const request = require('request');
const expect = require('chai').expect;
const chai = require('chai');
const sinon = require('sinon');
const assert = require("assert");
const mocha = require("mocha");

chai.use(require('sinon-chai'));

const esCrudApi = require('../app-modules/es-crud-api');

describe('CRUD elasticserach API', function () {

    it('Should create document', function (done) {

        let req = {
            body: {
                firstName: "first",
                lastName: "last",
                jobDescription: "janitor"
            }
        };

        let res = {
            send: sinon.spy()
        };

        let client = {
            create: sinon.spy()
        };

        let  api = {
            createDocument: sinon.spy()
        };

        let iSpy = sinon.spy(esCrudApi,"createDocument");

        api.createDocument(req,res);
        sinon.assert.called(api.createDocument);

        //expect(iSpy).to.have.been.calledWith(req,res);

        done();
    });


    it('Should get document by ID', function (done) {

        let req = {
            params :  {
                id: '100'
            }
        };

        let res = {
            send: sinon.spy()
        };

        let client = {
            get: sinon.spy()
        };

        let  api = {
            getDocumentById: sinon.spy()
        };

        api.getDocumentById(req,res);
        sinon.assert.called(api.getDocumentById);

        done();
    });

    it('Should get all documents', function (done) {

        let req = {};

        let res = {
            send: sinon.spy()
        };

        let client = {
            search: sinon.spy()
        };


        let  api = {
            getAllDocuments: sinon.spy()
        };


        api.getAllDocuments(req,res);
        sinon.assert.called(api.getAllDocuments);

        done();
    });

    it('Should update document by ID', function (done) {

        let req = {
            body: {
                firstName: "first",
                lastName: "last",
                jobDescription: "janitor"
            },
            params :  {
                id: '100'
            }
        };

        let res = {
            send: sinon.spy()
        };

        let client = {
            update: sinon.spy()
        };

        let  api = {
            updateDocument: sinon.spy()
        };

        api.updateDocument(req,res);
        sinon.assert.called(api.updateDocument);

        done();
    });


    it('Should delete document by ID', function (done) {

        let req = {
            params :  {
                id: '100'
            }
        };

        let res = {
            send: sinon.spy()
        };

        let client = {
            delete: sinon.spy()
        };

        let  api = {
            deleteDocument: sinon.spy()
        };

        api.deleteDocument(req,res);
        sinon.assert.called(api.deleteDocument);


        done();
    });

});
