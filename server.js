/**
 * there may be better ays to do this
 * to define the root app directory
 */
global.__basedir = __dirname;

// used for .env referenced in express-init.js
require('dotenv').config(); // default to .env in root directory

const express = require('./app-modules/express-init');
const esCrudApi = require('./app-modules/es-crud-api');
const mongoApi = require('./app-modules/mongo-api');
const credentials = require('./app-modules/credentials-api');

// load these so initialization happens
require('./app-modules/mongoose-init');
require('./app-modules/es-crud-init');

// database selection from client
const DB_SELECTION_MONGO = "mongo";
const DB_SELECTION_ELASTIC = "elastic";
const exp = express.exp;

//////////////////////////////////////////////////////
//
// Credential API's via mongoose
//
/////////////////////////////////////////////////////
exp.post('/credentials/register', (req, res) => {
    credentials.registerUser(req, res);
});

exp.post('/credentials/login', (req, res) => {
    credentials.loginUser(req, res);
});

exp.post('/credentials/logout', (req, res) => {
    credentials.logoutUser(req, res);
});

exp.get('/credentials/users', (req, res) => {
    credentials.getUsers(req, res);
});

exp.delete('/credentials/users/:id', (req, res) => {
    credentials.deleteUser(req, res);
});



//////////////////////////////////////////////////////
//
// API's for elasticsearch admin
//
/////////////////////////////////////////////////////

/**
 * Create an index
 */
exp.post('/elastic/createindex/:index', (req, res) => {
    esCrudApi.createIndex(req, res);
});


/**
 * Drop an index
 */
exp.delete('/elastic/dropindex/:index', (req, res) => {
    esCrudApi.dropIndex(req, res);
});


/**
 * Get all indices
 */
exp.get('/elastic/indices', (req, res) => {
    esCrudApi.getIndices(req, res);
});

/**
 * Get the document count
 */

exp.get('/elastic/documentcount', (req, res) => {
    esCrudApi.getDocumentCount(req, res);
});


/**
 * Create an Document
 */
exp.post('/accounts/person/:db', (req, res) => {
    let db = getSelectedDb(req);

    if (db === DB_SELECTION_ELASTIC) {
        esCrudApi.createDocument(req, res);
    }
    else if (db === DB_SELECTION_MONGO) {
        mongoApi.createDocument(req, res);
    }
    else {
        res.status(406).send({
            message: "invalid database selection"
        });
    }
});

/**
 * Get document by id
 */
exp.get('/accounts/person/:db/:id', (req, res) => {
    let db = getSelectedDb(req);

    if (db === DB_SELECTION_ELASTIC) {
        esCrudApi.getDocumentById(req, res);
    }
    else if (db === DB_SELECTION_MONGO) {
        mongoApi.getDocumentById(req, res);
    }
    else {
        res.status(406).send({
            message: "invalid database selection"
        });
    }
});

/**
 * Get all documents
 */
exp.get('/accounts/person/:db', (req, res) => {
    let db = getSelectedDb(req);

    if (db === DB_SELECTION_ELASTIC) {
        esCrudApi.getAllDocuments(req, res);
    }
    else if (db === DB_SELECTION_MONGO) {
        mongoApi.getAllDocuments(req, res);
    }
    else {
        res.status(406).send({
            message: "invalid database selection"
        });
    }
});

/**
 * Update a document
 *
 */
exp.put('/accounts/person/:db/:id', (req, res) => {
    let db = getSelectedDb(req);

    if (db === DB_SELECTION_ELASTIC) {
        esCrudApi.updateDocument(req, res);
    }
    else if (db === DB_SELECTION_MONGO) {
        mongoApi.updateDocument(req, res);
    }
    else {
        res.status(406).send({
            message: "invalid database selection"
        });
    }
});

/**
 *
 * Delete an entry
 *
 */
exp.delete('/accounts/person/:db/:id', (req, res) => {
    let db = getSelectedDb(req);

    if (db === DB_SELECTION_ELASTIC) {
        esCrudApi.deleteDocument(req, res);
    }
    else if (db === DB_SELECTION_MONGO) {
        mongoApi.deleteDocument(req, res);
    }
    else {
        res.status(406).send({
            message: "invalid database selection"
        });
    }
});

/**
 * Get the selected database
 *
 * @param req
 * @returns {*}
 */
const getSelectedDb = (req) => {
    let params = JSON.parse(JSON.stringify(req.params));
    return params.db;
};


/**
 * Some examples for REST API parameters, "query", "params" and "body"
 * using express format.
 *
 */

// /**
//  * ***  example path parameters  ***
//  *
//  * http://localhost:3000/users/34/books/8989
//  */
// exp.get('/users/:userId/books/:bookId', (req, res) => {
//     let obj = JSON.parse(JSON.stringify(req.params)); // path params
//     res.send(req.params)
// });
//
// /**
//  * *** example query  parameters ****
//  *
//  * http://localhost:3000/users?userId=89&bookId=5678
//  */
// exp.get('/users/', (req, res) => {
//     let obj = JSON.parse(JSON.stringify(req.query));  // query params
//     res.send(req.query)
// });
//
// /**
//  *  example pass in a json object  in the body
//  *  {
//  *	  "userId": "123",
//  *	  "bookId": "456789"
//  *  }
//  *
//  *  http://localhost:3000/users/data
//  */
// exp.post('/users/data', (req, res) => {
//     let obj = req.body;  // json obj in body
//
//     res.send(obj)
// });
