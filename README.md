# Project nodejs  application

This project implements a simple web application that uses a CRUD interface. There are two databases, Elasticsearch
and Mongodb, both use the same CRUD interface. From the web application the user can select which one
to interact with via the API


The CRUD implication uses Express on the nodejs server to both service up static files for the
web application and implement the CRUD API.


There are two sets of API's implemented on the nodejs server. One for a few elasticsearch admin
functions (see Additional Notes below) and another set for the CRUD web application.

The CRUD api is as follows:

```
db =  "elastic" | "mongo"

------------------------
Register for new login user
POST: /credentials/register
data: {
        userName: "user",
        password:  "password"
      }

------------------------
Login
POST: /credentials/login
data: {
        userName: "user",
        password:  "password"
      }

------------------------
Logout
POST: /credentials/logout
data: {
        userName: "user"
      }

------------------------
Create a new document
POST:  /accounts/person/{db}
data: {
        firstName: "first",
        lastName:  "lastName",
        jobDescription: "software developer"
      }

------------------------
Get document by ID
GET:  /accounts/person/{db}/{id}

------------------------
Get all documents
GET: /accounts/person/{db}

------------------------
Update  a document
PUT:  /accounts/person/{db}/{id}
data: {
        id: "12345",
        firstName: "first",
        lastName:  "lastName",
        jobDescription: "software developer"
      }

------------------------
Delete a document
DELETE:  /accounts/person/{db}/{id}

```

**NOTE 1**:  No authorization or authentication was implemented for either of the databases. You would need
to check the respective documentation for both. This project is meant as simple demo.

**NOTE 2**: This application was primarily build under Ubuntu Linux. Thus all the
commands to install the various artifacts are based on Linux commands.


## Getting Started

Update the package repository cache
```
sudo apt-get update
```

Install curl this will be used in the next step

```
sudo apt-get install curl
```

## Installing nodejs on Ubuntu 18

The steps below were taken from the below URL 
to allow to update to the most recent version of nodejs

https://www.digitalocean.com/community/tutorials/how-to-install-node-js-on-ubuntu-18-04

The basic steps are a follows:

Add nodejs PPA for  the Ubuntu repository


Replace the  below "x.x.x" with the nodejs version you want to install. 
This version is found at the nodejs home page  https://nodejs.org/.  I would
suggest going with the current LTS version 10.15.1

```
curl -sL https://deb.nodesource.com/setup_10.x -o nodesource_setup.sh

sudo bash nodesource_setup.sh
```

Now install nodejs

```
sudo apt install nodejs
```

Now check the current version you installed

$ nodejs --version

This should also have installed NPM

$ npm --version



## Clone the project

$ git clone https://gitlab.com/dgh1/nodejs-rest-web-app.git

Then install the required node_modules

```
NOTE: If you do not have yarn installed, here is a link for ubuntu: https://linuxize.com/post/how-to-install-yarn-on-ubuntu-18-04/
```

$ yarn install


# Installing Databases

##  Install elasticsearch database

See this link:

https://computingforgeeks.com/how-to-install-elasticsearch-6-x-on-ubuntu-18-04-lts-bionic-beaver-linux/

NOTE: you may not have to do Step 3 if the JDK is already installed on your machine.


The following are the basic steps:

```
wget -qO - https://artifacts.elastic.co/GPG-KEY-elasticsearch | sudo apt-key add -

echo "deb https://artifacts.elastic.co/packages/6.x/apt stable main" | sudo tee /etc/apt/sources.list.d/elastic-6.x.list

sudo apt update

sudo apt install elasticsearch

sudo systemctl daemon-reload

sudo systemctl enable elasticsearch.service

sudo systemctl restart elasticsearch.service


```



**default credential:** user/password  elastic/changeme

**default port:**  9200

Run below to make sure elasticserch is running
$ curl -u elastic:changeme localhost:9200
You should see something like:
```
{
  "name" : "8HMF27g",
  "cluster_name" : "my-application",
  "cluster_uuid" : "tbXXkPWDRDuDIoZm0n97dQ",
  "version" : {
    "number" : "6.5.1",
    "build_flavor" : "default",
    "build_type" : "deb",
    "build_hash" : "8c58350",
    "build_date" : "2018-11-16T02:22:42.182257Z",
    "build_snapshot" : false,
    "lucene_version" : "7.5.0",
    "minimum_wire_compatibility_version" : "5.6.0",
    "minimum_index_compatibility_version" : "5.0.0"
  },
  "tagline" : "You Know, for Search"
}
```

## Install Mongodb

See this link https://www.tecmint.com/install-mongodb-on-ubuntu-18-04/

The basic steps  are:

```
sudo apt update
sudo apt install mongodb
```

You can check if mongodb is running.

```
 sudo systemctl status mongodb
```

You should see something like:
```
 mongodb.service - An object/document-oriented database
   Loaded: loaded (/lib/systemd/system/mongodb.service; enabled; vendor preset: enabled)
   Active: active (running) since Wed 2018-12-12 06:03:39 PST; 1h 13min ago
     Docs: man:mongod(1)
 Main PID: 848 (mongod)
    Tasks: 24 (limit: 4915)
   Memory: 174.9M
   CGroup: /system.slice/mongodb.service
           `-848 /usr/bin/mongod --unixSocketPrefix=/run/mongodb --config /etc/mongodb.conf
```

If not you can start/stop mongodb.

```
sudo systemctl start mongodb
sudo systemctl stop mongodb
```

## The web application

The purpose of the web application is to be able to functionally do end-to-end testing of the REST API.

The web application code, html, css, images files, etc. is a React based application and
located  in the ./dist/webapp directory. The source for this in another 
gitlab project which you can install and make changes to try different things.

```
git clone  https://gitlab.com/dgh1/react-nodejs-crud-app.git
```


If you wish to run your own web application:
* In the .env file change DEV_TEST=false to DEV_TEST=true
* Under the ./dist create  new directory  ./dist/devtest
* Copy your application to ./dist/devtest
* Important! restart nodejs


## Logging

A logging capability is implemented using winston. For more information see  https://www.npmjs.com/package/winston

When the application starts up a "logs" directory is created in the project root directory if not already created.
The logs will rollover on a daily basis and there will not be any more than three log files.


##  Usage: Starting nodejs and launching the web application

After all the above dependencies have been installed you can start nodejs and open the web application in your browser.

**To start nodejs**
* cd to the root project directory
* nodejs server.js

You should see:

  
- elastic connected to http://localhost:9200
- web application listening on http://localhost:3000
- mongoose connect to mongoDB  port: 27017



You can change this port address in the file express-init.js  "const port = 3000;" and then  start nodejs.

**To open the web application**

Enter "http://localhost:3000"  in your browser or the port number you change to.

You should now be able to  Add, Update and Delete entries from the table. Clicking on an
entry in the table populates the GUI on the right.


The LOGIN button is mute, no real login is require just enter some data for 
name and password. 
~~~
NOTE: this is in clear text so don't enter any favorite password that
you would not want to be potentially compromised.
~~~ 
If you want to see the source code for this application:

https://gitlab.com/dgh1/react-nodejs-crud-app

##  Starting nodejs with HTTPS

To run in HTTPS mode change the .env file:

from  PRODUCTION=false

to    PRODUCTION=true

A self signed certificate is already installed.


## Additional Notes
If your familiar with the tool "postman" for testing API's and have it installed on your machine you
can import the collections file found in the ./misc directory. This will allow via postman
to call the elasticstore admin API's that were mentioned above.

## Unit Test
There are few tests but are rather weak and for now, mostly placeholders.  To run them:
* npm run test

To see the code coverage:
* npm run coverage

The results will be in  ./coverage/lcov-report

You can view the coverage in your web browser: 
file:///\<path to project\>/coverage/lcov-report/index.html

## Lint
To run lint:
* npm run lint

