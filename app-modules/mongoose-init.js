const logger = require('./logger');
const mongoose = require('mongoose');


//let ObjectId = require('mongodb').ObjectID;

/**
 *
 * initialize  mongoose
 *
 */
mongoose.connect('mongodb://localhost/test', {useNewUrlParser: true});
// see https://mongoosejs.com/docs/deprecations.html
mongoose.set('useFindAndModify', false);
mongoose.set('useCreateIndex', true);

let db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function () {
    console.log("- mongoose connect to mongoDB  port: " + db.port);
    logger.info("- mongoose connect to mongoDB  port: " + db.port);
});
