
const fs = require('fs');
const path = require('path');
const winston =  require('winston');
const DailyRotateFile = require('winston-daily-rotate-file');

/**
 *
 * Add some logging capability using winston
 *
 */
let logDir = './logs';
if (!fs.existsSync(logDir)){
    fs.mkdirSync(logDir);
}
const filename = path.join(global.__basedir, logDir,'logfile');
const logger = winston.createLogger({
    format: winston.format.combine(
        winston.format.timestamp({format:'YYYY/MM/DD  HH:mm:ss:SSS'}),
        winston.format.json()
    ),
    transports: [
        //new winston.transports.Console(),
        //new winston.transports.File({ filename }),
        new DailyRotateFile({
            datePattern: 'YYYY-MM-DD', // T = when to rotate
            filename: filename + "-%DATE%.log",
            maxFiles: 3,
            timestamp: true
        })
    ]
});

module.exports = logger;