const ACCOUNTS = "accounts";
const TYPE = "person";
const uuid = require('uuid/v4');
const logger = require('./logger');
const client = require('./es-crud-init');

/**
 * Create a newDocument
 *
 * @param req - HTTP request
 * @param res - HTTP response
 * @param client - elasticsearch client
 */
const createDocument = (req, res) => {
    logger.info("Elastic - createDocument() ");

    let data = req.body;

    let request = {
        refresh: "wait_for", // make sure doc is updated before retuning
        index: ACCOUNTS,
        type: TYPE,
        id: uuid(),
        body: {
            firstName: data.firstName,
            lastName: data.lastName,
            jobDescription: data.jobDescription
        }
    };

    let promise = new Promise(function (resolve, reject) {
        const status = client.getElasticClient().create(request);
        if (!status) {
            reject({
                error: "failed to create document"
            });
        }
        resolve(status);
    });

    promise.then(function (value) {
        res.status(200).send(value);
    }).catch((reason) => {
        logger.info(reason);
        res.status(500).send({
            message: "check nodejs logs"
        });
    });
};

/**
 * Return a document by  ID
 *
 * @param req - HTTP request
 * @param res - HTTP response
 * @param client - elasticsearch client
 */
const getDocumentById = (req, res) => {
    logger.info("Elastic - getDocumentById() ");

    let params = JSON.parse(JSON.stringify(req.params));

    let promise = new Promise(function (resolve, reject) {
        const status = client.getElasticClient().get({
            index: ACCOUNTS,
            type: TYPE,
            id: params.id
        });
        if (!status) {
            reject({
                error: "failed to get document by ID"
            });
        }
        resolve(status);
    });

    promise.then(function (doc) {
        res.status(200).send({
            id: doc._id,
            firstName: doc._source.firstName,
            lastName: doc._source.lastName,
            jobDescription: doc._source.jobDescription,
        });

    }).catch((reason) => {
        logger.info(reason);
        res.status(500).send({
            message: "check nodejs logs"
        });
    });
};

/**
 * Return all documents
 *
 * @param req - HTTP request
 * @param res - HTTP response
 * @param client - elasticsearch client
 */
const getAllDocuments = (req, res) => {
    logger.info("Elastic - getAllDocuments() ");

    let promise = new Promise(function (resolve, reject) {
        const status = client.getElasticClient().search({
            index: ACCOUNTS,
            size: 9000  // max records to return
        });
        if (!status) {
            reject({
                error: "failed to get all documents"
            });
        }
        resolve(status);
    });

    promise.then(function (value) {
        let docs = [];

        for (let doc of value.hits.hits) {
            docs.push({
                id: doc._id,
                firstName: doc._source.firstName,
                lastName: doc._source.lastName,
                jobDescription: doc._source.jobDescription,
            });
        }
        res.status(200).send(docs);
    }).catch((reason) => {
        logger.info(reason);
        res.status(500).send({
            message: "check nodejs logs"
        });
    });
};

/**
 * Update an  existing document by ID
 *
 * @param req - HTTP request
 * @param res - HTTP response
 * @param client - elasticsearch client
 */
const updateDocument = (req, res) => {
    logger.info("Elastic - updateDocument() ");

    let params = JSON.parse(JSON.stringify(req.params));
    let data = req.body;

    let promise = new Promise(function (resolve, reject) {
        const status = client.getElasticClient().update({
            index: ACCOUNTS,
            type: TYPE,
            id: params.id,
            refresh: "wait_for", // make sure doc is updated before retuning
            body: {
                doc: {
                    firstName: data.firstName,
                    lastName: data.lastName,
                    jobDescription: data.jobDescription
                }
            }
        });
        if (!status) {
            reject({
                error: "failed to update document"
            });
        }
        resolve(status);
    });

    promise.then(function (value) {
        res.status(200).send(value);
    }).catch((reason) => {
        logger.info(reason);
        res.status(500).send({
            message: "check nodejs logs"
        });
    });

};

/**
 * Delete a document by ID
 *
 * @param req - HTTP request
 * @param res - HTTP response
 * @param client - elasticsearch client
 */
const deleteDocument = (req, res) => {
    logger.info("Elastic - deleteDocument() ");

    let params = JSON.parse(JSON.stringify(req.params));

    let promise = new Promise(function (resolve, reject) {
        const status = client.getElasticClient().delete({
            refresh: "wait_for", // make sure doc is updated before retuning
            index: ACCOUNTS,
            type: TYPE,
            id: params.id
        });
        if (!status) {
            reject({
                error: "failed on deleting document"
            });
        }
        resolve(status);
    });

    promise.then(function (status) {
        res.status(200).send(status);
    }).catch((reason) => {
        logger.info(reason);
        res.status(500).send({
            message: "check nodejs logs"
        });
    });
};


/**
 *  Gets the number documents in elasticsearch
 *
 * @param req - HTTP request
 * @param res - HTTP response
 * @param client - elasticsearch client
 */
const getDocumentCount = (req, res) => {
    var promise = new Promise(function (resolve, reject) {
        const count = client.getElasticClient().count();
        if (!count) {
            reject({
                error: "failed to get document count"
            });
        }
        resolve(count);
    });

    promise.then(function (value) {
        res.send(value);
    }).catch((reason) => {
        res.send(reason);
    });

};

/**
 *  Get all the indices for elasticsearch
 *
 * @param req - HTTP request
 * @param res - HTTP response
 * @param client - elasticsearch client
 */
const getIndices = (req, res) => {

    var promise = new Promise(function (resolve, reject) {
        const status = client.getElasticClient().indices.get({
            index: "*"
        });
        if (!status) {
            reject({
                error: "failed to get indices"
            });
        }
        resolve(status);
    });

    promise.then(function (value) {
        res.send(value);
    }).catch((reason) => {
        let why = reason.toString();
        res.send({
            error: why
        });
    });
};

/**
 *  Drop the index
 *
 * @param req - HTTP request
 * @param res - HTTP response
 * @param client - elasticsearch client
 */
const dropIndex = (req, res) => {
    let obj = JSON.parse(JSON.stringify(req.params));

    var promise = new Promise(function (resolve, reject) {
        const status = client.getElasticClient().indices.delete({
            index: obj.index
        });
        if (!status) {
            reject({
                error: "failed to drop index"
            });
        }
        resolve(status);
    });

    promise.then(function (value) {
        res.send(value);
    }).catch((reason) => {
        let why = reason.toString();
        res.send({
            error: why
        });
    });
};

/**
 *  Create a new index
 *
 * @param req - HTTP request
 * @param res - HTTP response
 * @param client - elasticsearch client
 */
const createIndex = (req, res) => {
    let obj = JSON.parse(JSON.stringify(req.params));

    var promise = new Promise(function (resolve, reject) {
        const status = client.getElasticClient().indices.create({
            index: obj.index,
        });
        if (!status) {
            reject({
                error: "failed to create index"
            });
        }
        resolve(status);
    });

    promise.then(function (value) {
        res.send(value);
    }).catch((reason) => {
        let why = reason.toString();
        res.send({
            error: why
        });
    });

};

module.exports = {
    createDocument,
    getDocumentById,
    getAllDocuments,
    updateDocument,
    deleteDocument,
    // init,
    getDocumentCount,
    getIndices,
    dropIndex,
    createIndex
};
















